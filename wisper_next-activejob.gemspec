# frozen_string_literal: true

require_relative "lib/wisper_next/active_job/version"

Gem::Specification.new do |spec|
  spec.name = "wisper_next-activejob"
  spec.version = WisperNext::ActiveJob::VERSION
  spec.authors = ["Kris Leech"]
  spec.email = ["kris.leech@gmail.com"]

  spec.summary = ""
  spec.description = ""
  spec.homepage = ""
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  # spec.metadata["homepage_uri"] = spec.homepage
  # spec.metadata["source_code_uri"] = ""
  # spec.metadata["changelog_uri"] = ""

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "wisper_next"
  spec.add_dependency "activejob", '>= 4.0.0'
end
