# frozen_string_literal: true

require 'active_job'

module WisperNext
  module ActiveJob
    class Broadcaster
      def initialize(*)
      end

      def call(subscriber, event_name, payload)
        Wrapper.perform_later(subscriber.class.name, event_name, payload)
      end

      class Wrapper < ::ActiveJob::Base
        queue_as :default

        def perform(class_name, event_name, payload)
          class_name.constantize.new.public_send(event_name, payload)
        end
      end
    end
  end
end
