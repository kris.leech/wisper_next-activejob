# frozen_string_literal: true

module WisperNext
  module ActiveJob
    VERSION = "0.1.0"
  end
end
