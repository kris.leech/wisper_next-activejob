# frozen_string_literal: true

require_relative '../active_job/broadcaster'

# this is how WisperNext looks up broadcaster, this needs to change.
module WisperNext
  class Subscriber
    AsyncBroadcaster = ::WisperNext::ActiveJob::Broadcaster
  end
end
