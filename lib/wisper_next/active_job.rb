# frozen_string_literal: true

require_relative "active_job/version"
require 'active_job'
require 'wisper_next'
require_relative 'active_job/broadcaster'
require_relative 'subscriber/async_broadcaster' # what WisperNext looks for

module WisperNext
  module ActiveJob
    class Error < StandardError; end
  end
end
