# frozen_string_literal: true

RSpec.describe WisperNext::ActiveJob do
  it "has a version number" do
    expect(WisperNext::ActiveJob::VERSION).not_to be nil
  end

  let(:subscriber) do
    Class.new do
      include WisperNext.subscriber(:async)

      def user_created
        # noop
      end
    end.new
  end

  let(:adapter) { ActiveJob::Base.queue_adapter }

  before do
    ActiveJob::Base.queue_adapter = :test
    ActiveJob::Base.queue_adapter.enqueued_jobs.clear
  end

  it 'puts job on ActiveJob queue' do
    expect { subscriber.on_event('user_created', { id: 1 }) }.to change(adapter.enqueued_jobs, :size).by(1)
  end
end
