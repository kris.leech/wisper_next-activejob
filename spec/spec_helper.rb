# frozen_string_literal: true

require "pry"
require "wisper_next/activejob"

puts "Using ActiveJob version #{ActiveJob::VERSION::STRING}"
puts

RSpec.configure do |config|
  config.example_status_persistence_file_path = ".rspec_status"

  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
